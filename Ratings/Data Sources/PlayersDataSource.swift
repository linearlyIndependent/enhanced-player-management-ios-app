//
//  PlayersDataSource.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

// The datasource object managing the users for the view and view controller.
class PlayersDataSource: NSObject {
    //MARK: Properties
    
    var players: [PlayerDoc]
    
    override init() {
        self.players = PlayerDatabase.loadPlayerDocs()
        
        super.init()
    }
    
    var selectedPlayer: Player? {
        didSet {
            if let selectedPlayer = selectedPlayer, let index = self.players.firstIndex(where: {$0.data?.uuid == selectedPlayer.uuid }) {
                self.selectedPlayerIndex = index
            }
        }
    }
    
    var selectedPlayerIndex: Int?
    
    // MARK: Functions
    
    // Select a player.
    func selectPlayer(at indexPath: IndexPath) {
        self.selectedPlayer = self.players[indexPath.row].data
    }
    
    func numberOfPlayers() -> Int {
        self.players.count
    }
    
    // Append a player to the view.
    func append(player: Player, to tableView: UITableView) {
        let newPlayer = PlayerDoc(player: player)
        
        newPlayer.saveData()
        
        self.players.append(newPlayer)
        
        tableView.insertRows(at: [IndexPath(row: self.players.count-1, section: 0)], with: .automatic)
    }
    
    // Delete a player from the view.
    func deletePlayer(at indexPath: IndexPath) {
        let player = self.players[indexPath.row]
        
        self.players.remove(at: indexPath.row)
        player.deleteDoc()
    }
    
    // Returns the player at the given index.
    func player(at indexPath: IndexPath) -> Player {
        self.players[indexPath.row].data!
    }
    
    // Saves the player at the given index.
    func savePlayer(at indexPath: IndexPath) {
        self.players[indexPath.row].saveData()
    }
}
