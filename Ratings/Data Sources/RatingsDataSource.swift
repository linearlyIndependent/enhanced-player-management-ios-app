//
//  RatingsDataSource.swift
//  Ratings
//
//  Created by Telekomlab9 on 20.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import Foundation

// The datasource for the ratings.
class RatingsDataSource: NSObject {
    private var _selectedScore: Int?
    
    init(score: Int?) {
        super.init()
        
        self.selectedScore = score
    }
    
    //MARK: Properties
    let ratings = 1...5
    
    var selectedScore: Int? {
        get {
            return self._selectedScore
        }
        
        set(score) {
            if let newScore = score, self.ratings.contains(newScore) {
                self._selectedScore = newScore
            }
        }
    }
    
    //MARK: Functions
    func numberOfRatings() -> Int {
        self.ratings.count
    }
}
