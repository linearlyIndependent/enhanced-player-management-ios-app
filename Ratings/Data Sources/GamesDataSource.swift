//
//  GamesDataSource.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

// The data source for the games.
class GamesDataSource: NSObject {
    //MARK: Properties
    var games = [
        "Angry Birds",
        "Chess",
        "Russian Roulette",
        "Spin the Bottle",
        "Texas Hold'em Poker",
        "Tic-Tac-Toe",
        "Couch Surfing"]
    
    var selectedGame: String? {
        didSet {
            if let selectedGame = selectedGame, let index = games.firstIndex(of: selectedGame) {
                selectedGameIndex = index
            }
        }
    }
    
    var selectedGameIndex: Int?
    
    // MARK: Functions
    func selectGame(at indexPath: IndexPath) {
        selectedGame = self.games[indexPath.row]
    }
    
    func numberOfGames() -> Int {
        self.games.count
    }
    
    func gameName(at indexPath: IndexPath) -> String {
        self.games[indexPath.row]
    }
}
