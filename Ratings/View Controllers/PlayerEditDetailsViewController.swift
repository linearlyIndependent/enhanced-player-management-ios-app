//
//  PlayerEditDetailsViewController.swift
//  Ratings
//
//  Created by Telekomlab9 on 20.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

class PlayerEditDetailsViewController: UITableViewController, UITextFieldDelegate  {
    
    // Accepts any name with A-Za-z0-9' and any number of spaces between the name parts.
    private static let nameRegex: String = "^[A-Za-z0-9']+[A-Za-z0-9' ]*[A-Za-z0-9']+$"
    
    // True if adding a new player
    var newPlayer: Bool = true
    
    var player: Player?
    
    var game: String? = "" {
        didSet {
            self.gameLabel.text = self.game
        }
    }
    
    var rating: Int? = nil {
        didSet {
            self.ratingLabel.text = self.ratingName(rating: self.rating)
        }
    }
    
    var name: String? = "" {
        didSet {
            self.nametextField.text = self.name
        }
    }
    
    var validName: Bool = false {
        didSet {
            self.doneButton.isEnabled = self.validName
            
            if self.validName {
                self.nametextField.backgroundColor = UIColor.systemGreen
            } else {
                self.nametextField.backgroundColor = UIColor.systemRed
            }
        }
    }
    
    @IBOutlet weak var nametextField: UITextField!
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var titleBar: UINavigationItem!
    @IBOutlet weak var ratingCell: UITableViewCell!
    @IBOutlet weak var deleteCell: UITableViewCell!
    
    
    override func viewDidLoad() {
        self.game = self.player?.game ?? ""
        self.rating = self.player?.rating
        self.name = self.player?.name ?? ""
        //self.nametextField.text? = self.player?.name ?? ""
        self.validName = self.isValidName(name: self.name)
        
        if self.newPlayer {
            self.titleBar.title = "Add Player"
            self.ratingCell.isHidden = true
            self.deleteCell.isHidden = true
        } else {
            self.titleBar.title = "Edit Player"
            self.ratingCell.isHidden = false
            self.deleteCell.isHidden = false
        }
        
        self.nametextField.delegate = self
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Pass parameters to the main list view.

        switch segue.identifier {
        case "SaveModifiedPlayerDetail":
            if let playerName = self.nametextField.text, let gameName = self.gameLabel.text {
                if self.newPlayer {
                    // Adding a new player.
                    self.player = Player(name: playerName, game: gameName, rating: nil)
                } else {
                    // Editing existing player.
                    self.player?.name = self.name
                    self.player?.rating = self.rating
                    self.player?.game = self.game
                }
            }
            
        case "PickGame":
            // Pass parameters to the game chooser view.
            if let gamePickerViewController = segue.destination as? GamePickerViewController {
                gamePickerViewController.gamesDataSource.selectedGame = self.game
            }
        case "ChooseRating":
            if let ratingPickerViewController = segue.destination as? RatingPickerViewController {
                ratingPickerViewController.ratingsDataSource.selectedScore = self.rating
            }
        default:
             print("Default case reached in Playereditdetail prepare")
        }
    
    }
    
    func ratingName(rating: Int?) -> String {
        if let ratingNumber = rating {
            return "\(ratingNumber) stars"
        } else {
            return ""
        }
    }
    
    func isValidName(name: String?) -> Bool {
        if let nameString = name {
            return nameString.range(of: PlayerEditDetailsViewController.nameRegex, options: .regularExpression, range: nil, locale: nil) != nil
        } else {
            return false
        }
        
        //return !(name ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nametextField.resignFirstResponder()
        return true
    }
    
    @IBAction func editName(_ sender: UITextField) {
        self.validName = self.isValidName(name: self.nametextField.text)
        
        if self.validName {
            self.name = self.nametextField.text
        }
    }
}


extension PlayerEditDetailsViewController {
    @IBAction func unwindWithSelectedGame(segue: UIStoryboardSegue) {
        
        // Get the chosen game from the gamepicker viewcontroller.
        if let gamePickerViewController = segue.source as? GamePickerViewController, let selectedGame = gamePickerViewController.gamesDataSource.selectedGame {
            self.game = selectedGame
        }
    }
}

extension PlayerEditDetailsViewController {
    @IBAction func unwindWithSelectedRating(segue: UIStoryboardSegue) {
        
        // Get the chosen rating from the rating picker viewcontroller.
        if let ratingPickerViewController = segue.source as? RatingPickerViewController, let selectedRating = ratingPickerViewController.ratingsDataSource.selectedScore {
            self.rating = selectedRating
        }
    }
}
