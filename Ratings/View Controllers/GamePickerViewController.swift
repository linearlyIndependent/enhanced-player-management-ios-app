//
//  GamePickerViewController.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

class GamePickerViewController: UITableViewController {
    var gamesDataSource = GamesDataSource()
    
}

//MARK: Extensions

extension GamePickerViewController {
    // Gets the number of rows.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.gamesDataSource.numberOfGames()
    }
    
    // Creates the UI cell for the given row index.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath)
        cell.textLabel?.text = self.gamesDataSource.gameName(at: indexPath)
        
        if indexPath.row == gamesDataSource.selectedGameIndex {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
}

extension GamePickerViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let index = gamesDataSource.selectedGameIndex {
            let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
            cell?.accessoryType = .none
        }
        
        gamesDataSource.selectGame(at: indexPath)
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
        
        //This now updates the selected game and triggers the segue itself.
        performSegue(withIdentifier: "unwind", sender: cell)
    }
}
