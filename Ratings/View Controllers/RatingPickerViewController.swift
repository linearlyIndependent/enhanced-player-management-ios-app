//
//  RatingPickerViewController.swift
//  Ratings
//
//  Created by Telekomlab9 on 20.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

class RatingPickerViewController: UITableViewController {
    let ratingsDataSource = RatingsDataSource(score: nil)
    
    @IBOutlet weak var ratingsSegment: UISegmentedControl!
    @IBOutlet weak var ratingsImage: UIImageView!
    
    
    @IBAction func setRating(_ sender: UISegmentedControl) {
        let rating = self.ratingsSegment.selectedSegmentIndex + 1
        self.ratingsDataSource.selectedScore = rating
        
        if self.ratingsDataSource.selectedScore == rating {
            //Valid rating no errors.
            self.ratingsImage.image = UIImage(named: "\(rating)stars")
            performSegue(withIdentifier: "unwindRating", sender: self)
            
        } else {
            self.ratingsSegment.selectedSegmentIndex = (self.ratingsDataSource.selectedScore ?? 1)-1
        }
    }
    
    override func viewDidLoad() {
        if let rating = self.ratingsDataSource.selectedScore {
            self.ratingsSegment.selectedSegmentIndex = rating - 1
            self.ratingsImage.image = UIImage(named: "\(rating)stars")
        }
    }
}
