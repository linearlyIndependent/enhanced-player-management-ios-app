//
//  PlayersViewController.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

// This manages the view showing the list of users.
class PlayersViewController: UITableViewController {
    //MARK: Properties
    private var selectedIndex: Int = -1
    var playersDataSource = PlayersDataSource()
    
}

//MARK: Extensions
extension PlayersViewController {
    
    //Triggers the segue to the add/edit player view when clicking a player for editing.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.playersDataSource.selectPlayer(at: indexPath)
        
        let cell = tableView.cellForRow(at: indexPath)
        //cell?.accessoryType = .checkmark
        
        //This now updates the selected player and triggers the segue itself.
        self.performSegue(withIdentifier: "ToAddPlayer", sender: cell)
    }
    
    // Is called when a segue is triggered.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //print(123)
        //print(segue.identifier == "ToAddPlayer")
        //print(sender is UITableViewCell)
        //print(segue.destination)
        
        // Check if it is the segue to add/edit the player and if it was triggered by a cell.
        if segue.identifier == "ToAddPlayer" && sender is UITableViewCell {
            // Pass the player for editing to the target viewcontroller.
            if let navigationController = segue.destination as? UINavigationController, let playerEditViewController = navigationController.viewControllers[0] as? PlayerEditDetailsViewController {
            playerEditViewController.player = self.playersDataSource.selectedPlayer
            playerEditViewController.newPlayer = false
            }
        }
    }
}

extension PlayersViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.playersDataSource.numberOfPlayers()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
        cell.player = playersDataSource.player(at: indexPath)
        
        return cell
    }
}

extension PlayersViewController {

    @IBAction func cancelModifiedPlayerDetail(_ segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func saveModifiedPlayerDetail(_ segue: UIStoryboardSegue) {
            guard
                let playerEditDetailsViewController = segue.source as? PlayerEditDetailsViewController,
                let player = playerEditDetailsViewController.player
            else {
                return
            }
            
        //TODO: check here if new player and add only then
        if playerEditDetailsViewController.newPlayer {
            self.playersDataSource.append(player: player, to: self.tableView)
        } else {
            if let index = self.playersDataSource.selectedPlayerIndex {
                self.playersDataSource.savePlayer(at: IndexPath(row: index, section: 0))
                self.tableView.reloadRows(at: [IndexPath(item: index, section: 0)], with: UITableView.RowAnimation.automatic)
            }
            //self.tableView.reloadData()
        }
    }
    
    @IBAction func deletePlayer(_ segue: UIStoryboardSegue) {
        guard
            let playerEditDetailsViewController = segue.source as? PlayerEditDetailsViewController, playerEditDetailsViewController.player != nil
        else {
            return
        }
        
        if !playerEditDetailsViewController.newPlayer, let index = self.playersDataSource.selectedPlayerIndex {
            let indexPath = IndexPath(row: index, section: 0)
            
            self.playersDataSource.deletePlayer(at: indexPath)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
