//
//  Player.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import Foundation

// Represents one player.
class Player: NSObject, NSCoding {
    // Globally unique ID of the user.
    let uuid: UUID
    var name: String?
    var game: String?
    var rating: Int?
    
    required convenience init?(coder: NSCoder) {
        let uuid = coder.decodeObject(forKey: Player.Keys.uuid.rawValue) as! UUID
        let name = coder.decodeObject(forKey: Player.Keys.name.rawValue) as? String
        let game = coder.decodeObject(forKey: Player.Keys.game.rawValue) as? String
        let rating = coder.decodeObject(forKey: Player.Keys.rating.rawValue) as? Int
        
        self.init(uuid: uuid, name: name, game: game, rating: rating)
    }
    
    convenience init(name: String?, game: String?, rating: Int?) {
        self.init(uuid: UUID(), name: name, game: game, rating: rating)
    }
    
    private init(uuid: UUID, name: String?, game: String?, rating: Int?) {
        self.uuid = uuid
        self.name = name
        self.game = game
        self.rating = rating
    }
    
    // Interface implementation to allow the comparison of player objects based on UUID.
    static func == (lhs: Player, rhs: Player) -> Bool {
        return lhs.uuid == rhs.uuid
    }
    
    // Specifies how to encode a player.
    func encode(with coder: NSCoder) {
        coder.encode(self.uuid, forKey: Player.Keys.uuid.rawValue)
        coder.encode(self.name, forKey: Player.Keys.name.rawValue)
        coder.encode(self.game, forKey: Player.Keys.game.rawValue)
        coder.encode(self.rating, forKey: Player.Keys.rating.rawValue)
    }
    
    // The keys in the encoded key-value-pair document to given properties.
    private enum Keys: String {
        case uuid = "uuid"
        case name = "name"
        case game = "game"
        case rating = "rating"
    }
}
