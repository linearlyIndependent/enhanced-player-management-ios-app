//
//  PlayerCell.swift
//  Ratings
//
//  Created by Telekomlab9 on 19.12.19.
//  Copyright © 2019 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import UIKit

// Custom UITableViewCell for a player.
class PlayerCell: UITableViewCell {
    @IBOutlet weak var gameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!

    var player: Player? {
        didSet {
            guard let player = player else { return }
            
            self.gameLabel.text = player.game ?? "<no_game>"
            self.nameLabel.text = player.name ?? "<no_name>"
            self.ratingImageView.image = image(forRating: player.rating)
        }
    }
    
    private func image(forRating rating: Int?) -> UIImage? {
        
        if let ratingNumber = self.player?.rating {
            return UIImage(named: "\(ratingNumber)stars")
        } else {
            return nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
