//
//  PlayerDataBase.swift
//  Ratings
//
//  Created by Telekomlab9 on 14.01.20.
//  Copyright © 2020 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import Foundation

// Manages persisting to, reading from and managing of the user list data in local storage.
class PlayerDatabase: NSObject {
    // Puts together the path to the directory where the users are saved.
    static let privateDocsDir: URL = {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectoryURL = paths.first!.appendingPathComponent("PlayerDatabase")
        
        do {
            try FileManager.default.createDirectory(at: documentsDirectoryURL, withIntermediateDirectories: true, attributes: nil)
        } catch {
            print("Couldn't create directory.")
        }
        
        return documentsDirectoryURL
    }()
    
    // Load the player database from the saved docs.
    class func loadPlayerDocs() -> [PlayerDoc] {
        guard let files = try? FileManager.default.contentsOfDirectory(at: privateDocsDir, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) else { return [] }
        
        return files.filter{ $0.pathExtension == "player" }.map{ PlayerDoc(docPath: $0) }
    }
    
    // Creates a unique save path for a newly added user using UUIDs.
    class func nextPlayerDocPath(uuid: UUID) -> URL? {
        guard let files = try? FileManager.default.contentsOfDirectory(at: privateDocsDir, includingPropertiesForKeys: nil, options: .skipsHiddenFiles) else { return nil}
        
        // This should theoretically never happen.
        if files.contains(where: { $0.deletingPathExtension().lastPathComponent == uuid.uuidString }) {
            return nil
        }
        
        return privateDocsDir.appendingPathComponent("\(uuid.uuidString).player", isDirectory: true)
    }
}
