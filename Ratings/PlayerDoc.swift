//
//  PlayerDoc.swift
//  Ratings
//
//  Created by Telekomlab9 on 14.01.20.
//  Copyright © 2020 Patrik Ploechl. All rights reserved.
//
//  Based on:
//      https://www.raywenderlich.com/5055364-ios-storyboards-getting-started
//      https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more
//      https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data
//

import Foundation

// Reads and persists Player objects onto the local storage.
class PlayerDoc: NSObject {
    private var _data: Player?
    // The docpath of the file in which the player object is persisted.
    private var docPath: URL?
    
    var data: Player? {
        get {
            if self._data != nil { return self._data}
            
            // Read and save player data for first time if nil.
            let dataUrl = self.docPath!.appendingPathComponent(PlayerDoc.Keys.dataFile.rawValue)
            
            guard let codedData = try? Data(contentsOf: dataUrl) else  { return nil }
            
            self._data = try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(codedData) as? Player
            
            return self._data
        }
        set {
            self._data = newValue
        }
    }
    
    
    init(player: Player) {
        super.init()
        
        self._data = player
    }
    
    init(name: String?, game: String?, rating: Int?) {
        super.init()
    
        _data = Player(name: name, game: game, rating: rating)
    }
    
    init(docPath: URL) {
        super.init()
        self.docPath = docPath
    }
    
    // Generates the docpath of the user save file.
    func createDataPath() throws {
        guard docPath == nil else { return }
        
        docPath = PlayerDatabase.nextPlayerDocPath(uuid: self._data!.uuid)
        try FileManager.default.createDirectory(at: docPath!, withIntermediateDirectories: true, attributes: nil)
    }
    
    // Persists the user data.
    func saveData() {
        guard self.data != nil else { return }
        
        do {
            try self.createDataPath()
        } catch {
            print("error creating save dir: " + error.localizedDescription)
            return
        }
        
        let dataUrl = docPath!.appendingPathComponent(PlayerDoc.Keys.dataFile.rawValue)
        
        let codedData = try! NSKeyedArchiver.archivedData(withRootObject: self.data, requiringSecureCoding: false)
        
        do {
            try codedData.write(to: dataUrl)
        } catch {
            print("error writing to file: " + error.localizedDescription)
        }
    }
    
    // Deletes the user data from the storage.
    func deleteDoc() {
        if let docPath = self.docPath {
            do {
                try FileManager.default.removeItem(at: docPath)
            } catch {
                print("erro deleting folder: " + error.localizedDescription)
            }
        }
    }
    
    // Parts of the filename stored as enum keys for easier reference.
    enum Keys: String {
        case dataFile = "Data.plist"
    }
}
