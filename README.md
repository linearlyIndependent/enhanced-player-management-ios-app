# Enhanced Player Management iOS App

An iOS app created for the iPhone App Development Credit Course.

Based on:
*  [https://www.raywenderlich.com/5055364-ios-storyboards-getting-started](https://www.raywenderlich.com/5055364-ios-storyboards-getting-started)
*  [https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more](https://www.raywenderlich.com/5055396-ios-storyboards-segues-and-more)
*  [https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data](https://www.raywenderlich.com/6733-nscoding-tutorial-for-ios-how-to-permanently-save-app-data)
